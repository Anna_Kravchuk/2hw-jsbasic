// Отримати за допомогою модального вікна браузера дані користувача: ім'я та вік.
// Якщо вік менше 18 років - показати на екрані повідомлення: You are not allowed to visit this website.
// Якщо вік від 18 до 22 років (включно) – показати вікно з наступним повідомленням: Are you sure you want to continue? 
// і кнопками Ok, Cancel. Якщо користувач натиснув Ok, показати на екрані повідомлення: Welcome, + ім'я користувача. 
// Якщо користувач натиснув Cancel, показати на екрані повідомлення: You are not allowed to visit this website.
// Якщо вік більше 22 років – показати на екрані повідомлення: Welcome, + ім'я користувача.
// Обов'язково необхідно використовувати синтаксис ES6 (ES2015) для створення змінних.
// Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів ім'я, або при введенні віку 
// вказав не число - запитати ім'я та вік наново (при цьому дефолтним значенням для кожної зі 
// змінних має бути введена раніше інформація).


// 1.1промпт- запрашиваю имя
// 2. проверяю не пустая ли строка и  нуль и  намбр и на не НаН ( на нан  положит.не надо проверять--- НаН----!!!). Если все ок- 2промпт с запросом возраста
// 3. если пустая строка или  нуль или намбр или НаН - 1промт - запрашиваю имя (+дефолтное имя, указанное ранее -- как это делается?))

// 4. 2. промпт 2 - запрашиваю возраст
// 5. проверяю чтобы это было число    (?это-- +prompt())
// 6. если пустая строка или  нуль или НаН -  2промпт-запрашиваю возраст   (+дефолтный возраст, указаный ранее -- как это делается?)

// 7. если age 18<
// alert('You are not allowed');
// 8. если age 18>=, 22<= 
//  запрос подтверждения:
//     let reqwest= confirm ('Are you sure?')
//       if (reqwest) {
//           alert('Welcome' + userName)
//       }else{ alert('You are not allowed')}
// 9.все остальные случаи (age 22>=)
// alert ('Welcome' + userName);




//ОТВЕТЫ:


// 1. Які існують типи даних у Javascript?
// String, Number, Boolean, Undefined, NaN, null, BigInt, Object  та symbol 

// 2. У чому різниця між == і ===?
// Сурове та не сурове  дорівнює. Не  сурове == порівнює значення без приведення типів. Сурове ===  з перетворенням.

// 3. Що таке оператор?   
// Оператор- це  символ або інструкція, яка  визначає яку операцію потрібно  провести над операндами.



let userName = prompt('Your name');
while (userName === '' ||  userName === null || !isNaN(userName)) {   //пока тру- запускает тело
    userName = prompt('Your name', userName );
}

let userAge = prompt('Your age');
while (userAge === '' ||  userAge === null || isNaN(userAge)) {    //isNaN(userAge)-   проверка на преобразование строки в число
    userAge = prompt('Your age' , userAge);
}


console.log(userName ,  userAge);
userAge = +userAge
if (userAge <18) {
    alert('You are not allowed');
}
else if(userAge >= 18 && userAge <= 22){
    let reqwest = confirm('Are you sure?')
    if (reqwest){
        alert ('Welcome ' + userName)
    }else{ alert('You are not allowed')}

}else{ alert('Welcome ' + userName)};





//тут не корректно, не для проверки, но мне надо это сохранить как заметку 

// switch (userAge) {
//     case "18<" : {
//         alert('You are not allowed');
//         break;
//     }
//     case (userAge >= 18 && userAge <= 22): {
//         confirm('Are you sure?');
//         if true{                           // не понимаю, как разделить этап  на true  false
//         alert ('Welcome ' + userName)
//         }else{
//             alert('You are not allowed');
//         break;
//     }      
//     case "22>" : {
//         alert ('Welcome ' + userName);
//         break;
//     }
//     default:                                // не понимаю логики, как прописать дефолтное значение по условию
//         userAge=23;
//         alert ('Welcome ' + userName);
//     }
// }